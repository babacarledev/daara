package com.daara.back.service;

import com.daara.back.entities.Doc;
import com.daara.back.repositories.DocRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Service
@RequiredArgsConstructor
public class DocService {
    private final DocRepository documentRepository;

    public void addDocument(Doc doc) {
        documentRepository.save(doc);
    }

    public Doc findById(Long id) {
        return documentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Document  non trouvé"));
    }

    public List<Doc> getDocumentByIdEleve(Long idEleve) {
        return documentRepository.findAll().stream().filter(document -> Objects.equals(idEleve, document.getEleve().getId())).toList();
    }

    public void deleteById(Long id) {
        documentRepository.deleteById(id);
    }

}
