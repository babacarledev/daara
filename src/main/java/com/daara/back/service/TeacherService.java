package com.daara.back.service;

import com.daara.back.entities.Teacher;
import com.daara.back.repositories.TeacherRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {
    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public List<Teacher> findAll() {
        return this.teacherRepository.findAll();
    }

    public Optional<Teacher> findById(Long id) {
        return this.teacherRepository.findById(id);
    }

    public void addTeacher(Teacher teacher) {
        this.teacherRepository.save(teacher);
    }

    public Teacher updateTeacher(Teacher teacher) {
        return this.teacherRepository.save(teacher);
    }

    public void deleteTeacher(Long id) {
        this.teacherRepository.deleteById(id);
    }

}