package com.daara.back.service;

import com.daara.back.auth.AuthenticationRequest;
import com.daara.back.auth.AuthenticationResponse;
import com.daara.back.config.JwtService;
import com.daara.back.entities.User;
import com.daara.back.enums.Role;
import com.daara.back.repositories.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final DaaraService daaraService;

    @Transactional
    public void register(User utilisateur) {
        var user = User.builder()
                .lastname(utilisateur.getLastname())
                .firstname(utilisateur.getFirstname())
                .email(utilisateur.getEmail())
                .password(passwordEncoder.encode(utilisateur.getPassword()))
                .role(Role.USER)
                .daara(daaraService.addDaara(utilisateur.getDaara()))
                .build();
        userRepository.save(user);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(), request.getPassword()
                )
        );
        var user = userRepository.findByEmail(request.getEmail()).orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder().token(jwtToken).build();
    }
}
