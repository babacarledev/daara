package com.daara.back.service;

import com.daara.back.entities.Adresse;
import com.daara.back.exception.DaaraException;
import com.daara.back.repositories.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;

    public List<Adresse> findAll() {
        return this.addressRepository.findAll();
    }

    public Optional<Adresse> findById(Long id) {
        return this.addressRepository.findById(id);
    }

    public Adresse addAddress(Adresse adresse) {
        return this.addressRepository.save(adresse);
    }

    public Adresse updateAddress(Adresse adresse) {
        this.addressRepository.findById(adresse.getIdAdresse()).orElseThrow(() -> new DaaraException("l'address n'exite  pas"));
        return this.addressRepository.save(adresse);
    }

    public void deleteAddress(Long id) {
        this.addressRepository.deleteById(id);
    }
}
