package com.daara.back.service;

import com.daara.back.entities.School;
import com.daara.back.exception.DaaraException;
import com.daara.back.repositories.SchoolRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SchoolService {

    private final SchoolRepository schoolRepository;

    public List<School> findAll() {
        return this.schoolRepository.findAll();
    }

    public Optional<School> findById(Long id) {
        return this.schoolRepository.findById(id);
    }

    public void addSchool(School school) {
        this.schoolRepository.save(school);
    }

    public School updateSchool(School school) {
        this.schoolRepository.findById(school.getSchoolId()).orElseThrow(() -> new DaaraException("le daara n'existe pas"));
        return this.schoolRepository.save(school);
    }

    public void deleteSchool(Long id) {
        this.schoolRepository.deleteById(id);
    }
}
