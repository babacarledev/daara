package com.daara.back.service;

import com.daara.back.entities.Adresse;
import com.daara.back.entities.Daara;
import com.daara.back.entities.Eleve;
import com.daara.back.entities.User;
import com.daara.back.repositories.AddressRepository;
import com.daara.back.repositories.DaaraRepository;
import com.daara.back.repositories.EleveRepository;
import com.daara.back.repositories.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EleveService {

    private final EleveRepository eleveRepository;
    private final AddressRepository addressRepository;
    private final DaaraRepository daaraRepository;
    private final UserRepository userRepository;

    public List<Eleve> findAll() {
        return this.eleveRepository.findAll();
    }

    public Eleve findById(Long id) {
        return this.eleveRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Eleve no trouvé"));
    }

    @Transactional
    public void addELeve(Eleve eleve, Long daaraId) {
        Daara daara = daaraRepository.findById(daaraId).orElseThrow(() -> new EntityNotFoundException("Daara non trouvée"));
        eleve.setDaara(daara);
        Adresse adresse = this.addressRepository.save(eleve.getAdresse());
        eleve.setAdresse(adresse);
        this.eleveRepository.save(eleve);
    }

    @Transactional
    public Eleve updateELeve(Eleve eleve, Long daaraId) {
        Daara daara = daaraRepository.findById(daaraId).orElseThrow(() -> new RuntimeException("Daara non trouvée"));
        eleve.setDaara(daara);
        this.addressRepository.save(eleve.getAdresse());
        return this.eleveRepository.save(eleve);
    }

    @Transactional
    public void deleteELeve(Long id) {
        this.eleveRepository.deleteById(id);
    }

    public List<Eleve> findAllEleveByEmailUser(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(
                () -> new EntityNotFoundException("Eleve no existant")
        );
        return eleveRepository.findAllByDaara(user.getDaara());
    }
}
