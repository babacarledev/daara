package com.daara.back.service;

import com.daara.back.entities.Eleve;
import com.daara.back.repositories.DocRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service
@RequiredArgsConstructor
public class PdfService {

    private final EleveService eleveService;
    private final DocRepository docRepository;

    public byte[] generatePdfReport(Long idEleve) throws JRException, IOException {
        Eleve eleve = eleveService.findById(idEleve);
        Eleve[] eleveArray = {eleve};
        JRBeanArrayDataSource dataSource = new JRBeanArrayDataSource(eleveArray);

        // Chargement du fichier JRXML
        Resource resource = new ClassPathResource("report.jrxml");
        InputStream inputStream = resource.getInputStream();

        // Compilation du JRXML en mémoire et remplissage avec les données
        JasperPrint jasperPrint = JasperFillManager.fillReport(JasperCompileManager.compileReport(inputStream), null, dataSource);

        // Export du rapport rempli au format PDF
        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

}