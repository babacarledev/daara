package com.daara.back.service;

import com.daara.back.entities.Daara;
import com.daara.back.entities.Eleve;
import com.daara.back.entities.User;
import com.daara.back.exception.DaaraException;
import com.daara.back.repositories.AddressRepository;
import com.daara.back.repositories.DaaraRepository;
import com.daara.back.repositories.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class DaaraService {

    private final DaaraRepository daaraRepository;
    private final AddressRepository addressRepository;
    private final UserRepository userRepository;

    public List<Daara> findAll() {

        return this.daaraRepository.findAll();
    }

    public Optional<Daara> findById(Long id) {
        return this.daaraRepository.findById(id);
    }

    public Daara addDaara(Daara daara) {
        daara.setAdresse(this.addressRepository.save(daara.getAdresse()));
        return this.daaraRepository.save(daara);
    }

    @Transactional
    public Daara updateDaara(Daara daara) {
        this.addressRepository.save(daara.getAdresse());
        return this.daaraRepository.save(daara);
    }

    @Transactional
    public void deleteDaara(Long id) {
        Daara daara = this.findById(id).orElseThrow(
                () -> new DaaraException("Cet groupe n'existe pas "));
        this.daaraRepository.deleteById(id);
        addressRepository.deleteById(daara.getAdresse().getIdAdresse());
    }

    public List<Eleve> findByDaaraId(Long id) {
        return this.daaraRepository.findById(id).orElseThrow().getEleves();
    }

    public Daara findByEmail(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new EntityNotFoundException("Utilisateur  non trouvé"));
        return user.getDaara();
    }
}
