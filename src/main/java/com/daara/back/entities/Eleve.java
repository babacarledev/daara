package com.daara.back.entities;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table
public class Eleve {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull
    private String nom;
    @NonNull
    private String prenom;
    @NonNull
    private String sexe;
    private LocalDate dateDeNaissance;
    @NonNull
    private String tuteur;
    @ManyToOne(cascade = {
            CascadeType.ALL
    })
    @JoinColumn(name = "id_adresse")
    private Adresse adresse;
    @ManyToOne(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinColumn(name = "id_daara")
    private Daara daara;
}
