package com.daara.back.entities;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long schoolId;
    @NonNull
    private String schoolName;
    @NonNull
    private String responsable;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "addressId")
    private Adresse adresse;

}
