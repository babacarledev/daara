package com.daara.back.entities;

import com.daara.back.enums.TypeIdentity;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Getter
@Setter
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long teacherId;
    @NonNull
    private String lastName;
    @NonNull
    private String firstName;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dateOfBirth;
    @ManyToOne()
    private Adresse adresse;
    private String number;
    private String idIdentity;
    @NonNull
    private TypeIdentity typeIdentity;
}
