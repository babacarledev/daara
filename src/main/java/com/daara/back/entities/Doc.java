package com.daara.back.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Getter
@Setter
public class Doc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDocument;
    private String nomFichier;
    private String type;
    private Date dateDeCreation;
    @Lob
    @Column(length = 1000000)
    private byte[] fichier;
    @ManyToOne(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinColumn(name = "id_eleve")
    private Eleve eleve;
}