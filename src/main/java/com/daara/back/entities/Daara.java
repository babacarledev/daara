package com.daara.back.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Daara {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDaara;
    @NonNull
    private String nomDaara;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_adresse")
    private Adresse adresse;
    @OneToMany(
            mappedBy = "daara",
            cascade = CascadeType.REMOVE,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<Eleve> eleves = new ArrayList<>();
}
