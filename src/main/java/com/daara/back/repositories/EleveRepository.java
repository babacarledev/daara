package com.daara.back.repositories;

import com.daara.back.entities.Daara;
import com.daara.back.entities.Eleve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EleveRepository extends JpaRepository<Eleve, Long> {
    List<Eleve> findAllByDaara(Daara daara);

}
