package com.daara.back.specifications;

import com.daara.back.entities.School;
import com.daara.back.entities.School_;
import com.daara.back.params.SchoolSearchParams;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.jpa.domain.Specification;

public class SchoolSpecifications {
    public static Specification<School> combinParams(SchoolSearchParams schoolSearchParams) {
        return Specification.where(nameSchoolLike(schoolSearchParams.getName()));
    }

    private static Specification<School> nameSchoolLike(String parameter) {
        return (root, query, criteriaBuilder) -> {
            if (ObjectUtils.isEmpty(School_.SCHOOL_NAME)) {
                return criteriaBuilder.conjunction();
            }
            return null;
        };
    }
}