package com.daara.back.enums;

public enum Role {
    USER,
    ADMIN
}
