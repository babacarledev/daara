package com.daara.back.enums;

public enum TypeIdentity {
    PASSPORT,
    DRIVER_LICENSE,
    NATIONAL_ID,
    // Ajoutez d'autres types d'identité si nécessaire
}
