package com.daara.back.controller;

import com.daara.back.entities.Daara;
import com.daara.back.entities.Eleve;
import com.daara.back.service.DaaraService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/daara")
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class DaaraController {

    private final DaaraService daaraService;

    @GetMapping(value = {"", "/all"})
    public ResponseEntity<List<Daara>> getAllDaara() {
        List<Daara> daaras = daaraService.findAll();
        return ResponseEntity.ok(daaras);
    }

    @PostMapping("/add")
    public void addDaara(@RequestBody Daara daara) {
        this.daaraService.addDaara(daara);
    }

    @GetMapping("/{id}")
    public Daara getDaara(@PathVariable("id") Long id) {
        return this.daaraService.findById(id).orElseThrow(() -> new EntityNotFoundException("Daara Not found"));
    }

    @DeleteMapping("/{id}")
    public void deleteDaara(@PathVariable("id") Long id) {
        this.daaraService.deleteDaara(id);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Daara> updateDaara(@RequestBody Daara daara) {
        return ResponseEntity.ok(this.daaraService.updateDaara(daara));
    }

    @GetMapping(value = "/idDaara/{id}")
    public ResponseEntity<List<Eleve>> findByDaaraId(@PathVariable Long id) {
        return ResponseEntity.ok(daaraService.findByDaaraId(id));
    }

    @GetMapping(value = "/responsable/{email}")
    public ResponseEntity<Daara> findByDaaraId(@PathVariable String email) {
        return ResponseEntity.ok(daaraService.findByEmail(email));
    }
}