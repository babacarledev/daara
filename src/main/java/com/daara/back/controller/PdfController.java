package com.daara.back.controller;

import com.daara.back.entities.Doc;
import com.daara.back.entities.Eleve;
import com.daara.back.service.DocService;
import com.daara.back.service.EleveService;
import com.daara.back.service.PdfService;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.JRException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class PdfController {

    private final PdfService pdfService;
    private final DocService docService;
    private final EleveService eleveService;

    @GetMapping("/generate-pdf/{id}")
    public ResponseEntity<byte[]> generatePdf(@PathVariable("id") Long id) throws IOException, JRException {
        Doc doc = new Doc();
        byte[] pdfBytes = pdfService.generatePdfReport(id);
        Eleve eleve = eleveService.findById(id);
        doc.setEleve(eleve);
        doc.setFichier(pdfBytes);
        doc.setNomFichier("Attestation de Paiement");
        doc.setDateDeCreation(new Date());
        docService.addDocument(doc);
        //  HttpHeaders headers = pdfService.getHttpHeaders(pdfBytes);
        return new ResponseEntity<>(pdfBytes, HttpStatus.OK);
    }

}
