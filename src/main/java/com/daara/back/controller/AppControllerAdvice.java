package com.daara.back.controller;


import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
@Slf4j
public class AppControllerAdvice {

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = BadCredentialsException.class)
    public ProblemDetail runtimeException(BadCredentialsException exception) {
        logError(exception);
        return ProblemDetail.forStatusAndDetail(HttpStatus.UNAUTHORIZED, exception.getMessage());
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = AccessDeniedException.class)
    public ProblemDetail forbidden(AccessDeniedException exception) {
        logError(exception);
        return ProblemDetail.forStatusAndDetail(
                HttpStatus.FORBIDDEN, "Vos droits ne vous permettent pas de faire cet action");
    }

    private static void logError(Exception exception) {
        AppControllerAdvice.log.error(exception.getMessage(), exception);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {ExpiredJwtException.class, MalformedJwtException.class})
    public ProblemDetail tokenException(Exception exception) {
        logError(exception);
        return ProblemDetail.forStatusAndDetail(HttpStatus.UNAUTHORIZED, "Token invalide");
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = RuntimeException.class)
    public ProblemDetail runtimeException(RuntimeException exception) {
        logError(exception);
        return ProblemDetail.forStatusAndDetail(HttpStatus.UNAUTHORIZED, exception.getMessage());
    }
}