package com.daara.back.controller;

import com.daara.back.entities.Teacher;
import com.daara.back.service.TeacherService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/daara/teacher")
public class TeacherController {
    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping(value = {"", "/all"})
    public ResponseEntity<List<Teacher>> getAllTeacher() {
        List<Teacher> listTeachers = teacherService.findAll();
        return ResponseEntity.ok(listTeachers);
    }

    @PostMapping("/add")
    public void addTeacher(@RequestBody Teacher teacher) {
        this.teacherService.addTeacher(teacher);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Teacher>> getATeacher(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.teacherService.findById(id));
    }

    @DeleteMapping("/{id}")
    public void deleteTeacher(@PathVariable("id") Long id) {
        this.teacherService.deleteTeacher(id);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Teacher> updateTeacher(@RequestBody Teacher teacher) {
        return ResponseEntity.ok(this.teacherService.updateTeacher(teacher));
    }

}
