package com.daara.back.controller;

import com.daara.back.entities.Adresse;
import com.daara.back.exception.DaaraException;
import com.daara.back.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("api/address")
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class AdresseController {
    private final AddressService addressService;
    @GetMapping(value = {"", "/all"})
    public ResponseEntity<List<Adresse>> getAllAddress() {
        List<Adresse> adresses = addressService.findAll();
        return ResponseEntity.ok(adresses);
    }

    @PostMapping("/add")
    public Adresse addAddress(@RequestBody Adresse adresse) {
        return this.addressService.addAddress(adresse);
    }

    @GetMapping("/{id}")
    public Adresse getAddress(@PathVariable("id") Long id) {
        return this.addressService.findById(id).orElseThrow(() -> new DaaraException("Address Not found") );
    }

    @DeleteMapping("/{id}")
    public void deleteAddress(@PathVariable("id") Long id) {
        this.addressService.deleteAddress(id);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Adresse> updateAddress(@RequestBody Adresse adresse) {
        return ResponseEntity.ok(this.addressService.updateAddress(adresse));
    }

}
