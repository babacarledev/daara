package com.daara.back.controller;

import com.daara.back.entities.Eleve;
import com.daara.back.exception.DaaraException;
import com.daara.back.service.EleveService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/daara/student")
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class EleveController {
    private final EleveService eleveService;

    @GetMapping(value = {"", "/all"})
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<Eleve>> getAllELeve() {
        return ResponseEntity.ok(eleveService.findAll());
    }

    @PostMapping("{daaraId}/add")
    public void addELeve(@RequestBody Eleve eleve, @PathVariable Long daaraId) {
        this.eleveService.addELeve(eleve, daaraId);
    }

    @GetMapping("/{id}")
    public Eleve getAELeve(@PathVariable("id") Long id) {
        return this.eleveService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteELeve(@PathVariable("id") Long id) {
        this.eleveService.deleteELeve(id);
    }

    @PutMapping(value = "{daaraId}/update")
    public ResponseEntity<Eleve> updateELeve(@RequestBody Eleve eleve, @PathVariable Long daaraId) {
        return ResponseEntity.ok(this.eleveService.updateELeve(eleve, daaraId));
    }

    @GetMapping("/allbyemailuser/{email}")
    public ResponseEntity<List<Eleve>> findAllByEmailUser(@PathVariable String email) {
        return ResponseEntity.ok(eleveService.findAllEleveByEmailUser(email));
    }
}
