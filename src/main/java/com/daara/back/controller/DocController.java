package com.daara.back.controller;


import com.daara.back.entities.Doc;
import com.daara.back.service.DocService;
import com.daara.back.service.EleveService;
import com.daara.back.service.PdfService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;

@RestController
@RequestMapping("api/document")
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class DocController {
    private final DocService docService;
    private final PdfService pdfService;
    private final EleveService eleveService;

    @PostMapping("/add/{idEleve}")
    public void addDocument(@RequestBody Doc file, @PathVariable Long idEleve) throws IOException {
        Doc doc = new Doc();
        doc.setFichier(file.getFichier());
        doc.setEleve(eleveService.findById(idEleve));
        doc.setDateDeCreation(new Date());
        docService.addDocument(doc);
    }

    @GetMapping("/eleve/{idEleve}")
    public ResponseEntity<byte[]> findDocumentByIdEleve(@PathVariable Long idEleve) {
        try {
            byte[] report = pdfService.generatePdfReport(idEleve);
            if (report != null) {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_PDF);
                headers.setContentDispositionFormData("attachment", "report.pdf");
                return new ResponseEntity<>(report, headers, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
