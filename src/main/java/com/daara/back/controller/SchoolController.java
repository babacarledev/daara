package com.daara.back.controller;

import com.daara.back.entities.School;
import com.daara.back.exception.DaaraException;
import com.daara.back.service.SchoolService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/daara/school")
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class SchoolController {
 private final SchoolService schoolService;

    @GetMapping(value = {"", "/all"})
    public ResponseEntity<List<School>> getAllSchool() {
        List<School> schhols = schoolService.findAll();
        return ResponseEntity.ok(schhols);
    }

    @PostMapping("/add")
    public void addSchool(@RequestBody School school) {
        this.schoolService.addSchool(school);
    }

    @GetMapping("/{id}")
    public School getSchool(@PathVariable("id") Long id) {
        return this.schoolService.findById(id).orElseThrow(() -> new DaaraException("School Not found") );
    }

    @DeleteMapping("/{id}")
    public void deleteSchool(@PathVariable("id") Long id) {
        this.schoolService.deleteSchool(id);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<School> updateDaara(@RequestBody School school) {
        return ResponseEntity.ok(this.schoolService.updateSchool(school));
    }
}
