package com.daara.back.exception;

public class DaaraException extends RuntimeException {
    public DaaraException(String message) {
        super(message);
    }

    public DaaraException(Exception e) {
        super(e.getMessage());
    }
}
