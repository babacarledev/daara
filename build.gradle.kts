plugins {
    java
    id("org.springframework.boot") version "3.1.3"
    id("io.spring.dependency-management") version "1.1.3"
    id("org.sonarqube") version "4.2.1.3168"
    id("org.openapi.generator") version "6.6.0"
}

group = "com.daara"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    compileOnly("org.projectlombok:lombok")
    annotationProcessor("org.projectlombok:lombok")
    implementation("com.github.javafaker:javafaker:1.0.2")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    implementation("org.mapstruct:mapstruct:1.5.5.Final")
    annotationProcessor("org.mapstruct:mapstruct-processor:1.5.5.Final")
    annotationProcessor("org.hibernate.orm:hibernate-jpamodelgen:6.2.7.Final")

    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("io.springfox:springfox-boot-starter:3.0.0")
    implementation("com.mysql:mysql-connector-j:8.2.0")
    implementation("io.jsonwebtoken:jjwt-api:0.11.5")
    implementation("io.jsonwebtoken:jjwt-impl:0.11.5")
    implementation("io.jsonwebtoken:jjwt-jackson:0.11.5")
    implementation("org.springframework.security:spring-security-test")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("com.github.librepdf:openpdf:1.3.30")
    implementation("net.sf.jasperreports:jasperreports:6.20.6")
    implementation("org.postgresql:postgresql")

}

sonar {
    properties {
        property("sonar.projectKey", "babacarledev_daara")
        property("sonar.organization", "babacarledev")
    }
}
configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

buildscript {
    repositories {
        mavenLocal()
        mavenCentral()
    }
    dependencies {
        classpath("org.openapitools:openapi-generator-gradle-plugin:7.0.0")
    }
}

apply(plugin = "org.openapi.generator")

openApiGenerate {
    generatorName.set("spring")
    inputSpec.set("$rootDir/src/main/openapi/openapi.yml")
    outputDir.set("$buildDir/generated")
    apiPackage.set("com.daara.back.api")
    invokerPackage.set("com.daara.back.invoker")
    modelPackage.set("com.daara.back.model")
}

repositories {
    mavenCentral()
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.compileJava {
    dependsOn("openApiGenerate")
}