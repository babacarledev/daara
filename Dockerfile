# Utilisation d'une image Java de base
FROM openjdk:latest

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier le jar Spring Boot dans le conteneur
COPY build/libs/daara-0.0.1-SNAPSHOT.jar /app/daara.jar

# Commande pour exécuter l'application Spring Boot
CMD ["java", "-jar", "daara.jar"]